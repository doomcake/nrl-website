# NRL 7442 - Geospatial Computing Website#

### Details ###

Angular.js app for displaying static site content, as defined by the sitemap.json file. 

Uses Twitter bootstrap for general layout

### Set up ###

* Site is already configured and ready to deploy, just copy it into the desired directory.
+ All pages are simple html files contained in the /content directory
- Page information (Title, Id, Filename, and optional settings are all defined in sitemap.json

### sitemap.json details ###

sitemap.json contains an array of page objects. It is hierarchical- a page object can contain an array of page objects. 

*Required Attributes:*

*  id: unique, url-safe identifier. Content for "id":"about" would be stored in /content/about.html
*  name: page name

*Optional attributes:*

* custom (Boolean): page content area is full-width and non-styled. This is useful for complicated layouts (homepage) or extra-wide data tables
* sidebar (String): displays a left-hand sidebar menu containing a page's pages. For example, "sidebar":"about" will display a menu for all the pages contained in about's "pages" array.
* banner (filename): displays a full witdth banner at top of page, using filename specified as the background. Recommended banner size is: 1920x300.
* pages (array of page objects) - child pages


### Contact ###

* Daniel Murphy
* dtmurph1@uno.edu